func makeIncrementer(incrementAmount: Int) -> (Int) -> Int {
	{ num in num + incrementAmount }
}

let tenIncrementer = makeIncrementer(incrementAmount: 10)
print(tenIncrementer(10)) //20
print(tenIncrementer(3))  //13
